#!/usr/bin/env python
from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import pickle
import os


# Convert images to gif
# convert -delay 50 -loop 0 figures/*.png adjustment.gif
def save_image(output_images_dir,x,y,w,b,i):
    plt.clf()
    y2 = w*x + b
    axis_size = np.arange(-10, 10, .5)
    w = int(w * 100)/100.
    b = int(b * 100)/100.
    func_label = 'y = ' + str(w) + 'x + ' + str(b)
    linear_function = plt.plot(x,y2, 'k--', label=func_label)
    noisy_points = plt.plot(x,y,'bo', label='y with noise')
    x_axis = plt.plot(axis_size, 0*axis_size, 'k')
    y_axis = plt.plot(0*axis_size, axis_size, 'k')
    plt.legend(loc=2)

    iteration = str(i).zfill(5)
    plt.title( 'iteration = ' + iteration )
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis([-5, 5, -4, 8])
    plt.grid(True)
    file_name = 'weights_adjustment_' + iteration + '.png'
    output_image_path = os.path.join( output_images_dir, file_name )
    plt.savefig( output_image_path, format='png' )
    return

output_images_dir = 'figures'
if not os.path.exists( output_images_dir ):
    os.mkdir( output_images_dir )


# Perceptron
# Sum of weights and bias:
# h(x) = sum(wx) + b

# Activation function:
# There are several activation functions
# Rectifier linear unit (ReLU):
# f(x) = 0 ; x < 0 
# f(x) = x ; x >= 0
# There is no activation function in this example


# Read datset file
file_name = 'dataset.p'
data = pickle.load( open( file_name, 'rb' ) )
data = np.vsplit( data, 2 )
y = data[0][0]
x = data[1][0]


# 1 input perceptron (x)
# h = w*x + b

# We must adjust w to reduce the the loss/error/objective to a local minimum
# Loss funcion: Mean Squared Error (MSE)
# Error = 1/N * sum( (y - h)^2 )
# https://en.wikipedia.org/wiki/Mean_squared_error

# Gradient descent
# We compute the derivetive regarding each weight (w) or bias (b)
# Derivative = - 2/N * sum( y - h )*x

# Weights initial value = 0
w = 0
b = 0

# Learning rate
lr = 0.01
num_points = x.size
num_iterations = 300

save_image(output_images_dir,x,y,w,b,0)

# Starts iterations from 1
for i in range(1, num_iterations + 1):
    h = np.zeros(num_points)
    mse = 0
    avg_error = 0
    derivative_w = 0
    derivative_b = 0
    # Iterative solution
    for j in range(num_points):
        h[j] = w * x[j] + b
        mse += (y[j] - h[j]) ** 2
        avg_error += (y[j] - h[j])

        # Derivative for w and b
        derivative_w += (y[j] - h[j])*x[j]
        derivative_b += (y[j] - h[j])

    mse /= num_points
    avg_error /= num_points
    derivative_w *= - (2./num_points)
    derivative_b *= - (2./num_points)

    # Update weights:
    w = w - lr * derivative_w
    b = b - lr * derivative_b

    if i == 1:
        save_image(output_images_dir,x,y,w,b,i)
    if i % 10 == 0:
        print( 'iteration =', i, ', avg_error =', avg_error, ', mse = ', mse )
        save_image(output_images_dir,x,y,w,b,i)

print( 'Final weight =', w )
print( 'Final bias =', b )
print( 'y = {}*x + {}'.format(w, b)  )

