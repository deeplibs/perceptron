#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import pickle


# Perceptron
# Sum of weights and bias:
# h(x) = sum(wx) + b

# Activation function:
# There are several activation functions
# Rectifier linear unit (ReLU):
# f(x) = 0 ; x < 0
# f(x) = x ; x >= 0
# There is no activation function in this example

# Load datset file
file_name = 'dataset.p'
data = pickle.load( open( file_name, 'rb' ) )
data = np.vsplit( data, 2 )
y = data[0][0]
x = data[1][0]


# 1 input perceptron (x)
# h = w*x + b

# We must adjust w to reduce the the loss/error/objective to a local minimum
# Loss funcion: Mean Squared Error (MSE)
# Error = 1/N * sum( (y - h)^2 )
# https://en.wikipedia.org/wiki/Mean_squared_error

# Gradient descent
# We compute the derivetive regarding each weight (w) or bias (b)
# Derivative = - 2/N * sum( y - h )*x


# Initial value = 0
w = .0
b = .0

# Learning rate
lr = 0.01
num_points = x.size
num_iterations = 1000


# Starts iterations from 1
for i in range(1, num_iterations + 1):
    h = w*x + b
    avg_error = np.mean( y - h )

    # Derivative for w and b
    derivative_w = - (2./num_points) * np.sum( (y - h)*x )
    derivative_b = - (2./num_points) * np.sum( (y - h) )

    # Update weights:
    w = w - lr * derivative_w
    b = b - lr * derivative_b

    if i % 100 == 0:
        print( 'iteration =', i, ', avg_error =', avg_error )

print( 'Final weight =', w )
print( 'Final bias =', b )
print( 'y = {}*x + {}'.format(w, b)  )

