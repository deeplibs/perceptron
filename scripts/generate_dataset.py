#!/usr/bin/env python
from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import pickle


# Original linear function
# function y = ax + b
a = 1
b = 2
x = np.arange(-5, 5, .2, dtype=float)
y = a * x + b

# Generate gaussian noise (normal distribution)
mean = 0.0
sigma = 0.5
num_points = x.size
noise = np.random.normal(mean, sigma, num_points)

# Add noise to output
y2 = y + noise

# Save data
data = np.stack( (y2,x), axis=0)
dataset_name = 'dataset.p'
pickle.dump( data, open( dataset_name, "wb" ) )


# Plot data
axis_size = np.arange(-10, 10, .5)
func_label = 'y = ' + str(a) + 'x + ' + str(b)
linear_function = plt.plot(x,y, 'k--', label=func_label)
noisy_points = plt.plot(x,y2,'bo', label='y with noise')
x_axis = plt.plot(axis_size, 0*axis_size, 'k')
y_axis = plt.plot(0*axis_size, axis_size, 'k')
plt.legend(loc=2)

plt.xlabel('X')
plt.ylabel('Y')
plt.axis([-5, 5, -4, 8])
plt.grid(True)

file_name = 'gaussian_noise.png'
plt.savefig( file_name, format='png' )

plt.show()


