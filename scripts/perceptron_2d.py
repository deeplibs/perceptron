#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import pickle


# Perceptron
# Sum of weights and bias:
# h(x) = sum(wx) + b


# Read datset file
file_name = 'dataset_2d.p'
data = pickle.load( open( file_name, 'rb' ) )
data = np.vsplit( data, 3 )
y = data[0]
x1 = data[1]
x2 = data[2]


# 2 input perceptron
# h = w1*x1 + w2*x2 + b


# We must adjust w to reduce the the loss/error/objective to a local minimum
# Loss funcion: Mean Squared Error (MSE)
# Error = 1/N * sum( (y - h)^2 )
# https://en.wikipedia.org/wiki/Mean_squared_error

# Gradient descent
# We compute the derivetive regarding each weight (w) or bias (b)
# Derivative = - 2/N * sum( y - h )*x

# Weights initial value = 0
w1 = .0
w2 = .0
b  = .0

# Learning rate
lr = 0.01
num_points = y.size
num_iterations = 1000


# Starts iterations from 1
for i in range(1, num_iterations + 1):
    h = w1*x1 + w2*x2 + b
    avg_error = np.mean( y - h )

    # Derivative for w and b
    derivative_w1 = - (2./num_points) * np.sum( (y - h)*x1 )
    derivative_w2 = - (2./num_points) * np.sum( (y - h)*x2 )
    derivative_b  = - (2./num_points) * np.sum( (y - h) )

    # Update weights:
    w1 -= lr * derivative_w1
    w2 -= lr * derivative_w2
    b  -= lr * derivative_b

    if i % 100 == 0:
        print( 'iteration =', i, ', avg_error =', avg_error)

print( 'Final weights = {}, {}, {}'.format(w1, w2, b) )
print( 'y = {}*x1 + {}*x2 + {}'.format(w1, w2, b)  )

